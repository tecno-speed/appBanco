var db;
var usuarios = [];
var editando = -1;

document.addEventListener('deviceready', onDeviceready)

function onDeviceready() {
    db = sqlitePlugin.openDatabase({ name: 'mBanco.db' });

    db.transaction(function (txn) {
        // var sql = 'drop table usuarios'

        var sql = 'create table if not exists usuarios (id integer primary key, nome text, telefone text)'
        txn.executeSql(sql)

        listar();
    });
}

function salvar() {
    var nome = document.getElementById('nome').value;
    var telefone = document.getElementById('telefone').value;

    db.transaction(function (txn) {

        if (editando > 0) {
            var sql = 'update usuarios set nome = ?, telefone = ? where (id = ' + editando + ')'
        } else {
            var sql = 'insert into usuarios (nome, telefone) VALUES (?, ?)'
        }

        txn.executeSql(sql, [nome, telefone],
            (tx, onSuccess) => {

                listar();
                document.getElementById('nome').value = '';
                document.getElementById('telefone').value = '';
                editando = -1;

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });
}

function listar() {
    usuarios = [];

    db.transaction(function (txn) {
        var sql = 'select * from usuarios'

        txn.executeSql(sql, [],
            (tx, onSuccess) => {

                for (var i = 0; i < onSuccess.rows.length; i++) {
                    usuarios.push(onSuccess.rows.item(i));
                }

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });
}

function getByID(id) {

    return new Promise((resolve, reject) => {

        db.transaction(function (txn) {
            var sql = 'select * from usuarios where (id = ?)'

            txn.executeSql(sql, [id],
                (tx, onSuccess) => {

                    resolve(onSuccess.rows.item(0))

                }, (tx, onError) => {
                    console.log(tx, onError)
                    reject(null);
                });
        });

    });
}

function editar(id) {

    getByID(id).then((rows) => {

        editando = rows['id'];
        document.getElementById('nome').value = rows['nome'];
        document.getElementById('telefone').value = rows['telefone'];

    });

}

function excluir(id) {
    db.transaction(function (txn) {
        var sql = 'delete from usuarios where (id = ?)'

        txn.executeSql(sql, [id],
            (tx, onSuccess) => {

                listar();

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });
}